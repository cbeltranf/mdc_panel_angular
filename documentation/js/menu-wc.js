'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">default documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="dependencies.html" data-type="chapter-link">
                                <span class="icon ion-ios-list"></span>Dependencies
                            </a>
                        </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse" ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AccordionControlModule.html" data-type="entity-link">AccordionControlModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AccordionControlModule-2307998bae92a51904afb391b820d841"' : 'data-target="#xs-components-links-module-AccordionControlModule-2307998bae92a51904afb391b820d841"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AccordionControlModule-2307998bae92a51904afb391b820d841"' :
                                            'id="xs-components-links-module-AccordionControlModule-2307998bae92a51904afb391b820d841"' }>
                                            <li class="link">
                                                <a href="components/AccordionControlComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionControlComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-AccordionControlModule-2307998bae92a51904afb391b820d841"' : 'data-target="#xs-directives-links-module-AccordionControlModule-2307998bae92a51904afb391b820d841"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-AccordionControlModule-2307998bae92a51904afb391b820d841"' :
                                        'id="xs-directives-links-module-AccordionControlModule-2307998bae92a51904afb391b820d841"' }>
                                        <li class="link">
                                            <a href="directives/AccordionControlPanelContentDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionControlPanelContentDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/AccordionControlPanelDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionControlPanelDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/AccordionControlPanelTitleDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionControlPanelTitleDirective</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-2ad92e2c8241bd6e4745c18a9073ff19"' : 'data-target="#xs-components-links-module-AppModule-2ad92e2c8241bd6e4745c18a9073ff19"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-2ad92e2c8241bd6e4745c18a9073ff19"' :
                                            'id="xs-components-links-module-AppModule-2ad92e2c8241bd6e4745c18a9073ff19"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-2ad92e2c8241bd6e4745c18a9073ff19"' : 'data-target="#xs-injectables-links-module-AppModule-2ad92e2c8241bd6e4745c18a9073ff19"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-2ad92e2c8241bd6e4745c18a9073ff19"' :
                                        'id="xs-injectables-links-module-AppModule-2ad92e2c8241bd6e4745c18a9073ff19"' }>
                                        <li class="link">
                                            <a href="injectables/AclService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AclService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ClassInitService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ClassInitService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ClipboardService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ClipboardService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/CompanyService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>CompanyService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/DataTableService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>DataTableService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/HeaderService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>HeaderService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LayoutConfigService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LayoutConfigService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LayoutConfigStorageService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LayoutConfigStorageService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LayoutRefService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LayoutRefService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LogsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LogsService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MenuAsideService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MenuAsideService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MenuConfigService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MenuConfigService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MenuHorizontalService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MenuHorizontalService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MessengerService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MessengerService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/PageConfigService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>PageConfigService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/QuickSearchService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>QuickSearchService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/SplashScreenService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>SplashScreenService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/SubheaderService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>SubheaderService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UserService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UtilsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UtilsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthenticationModule.html" data-type="entity-link">AuthenticationModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthenticationModule-4324a033f778707528df3ec698bc1721"' : 'data-target="#xs-injectables-links-module-AuthenticationModule-4324a033f778707528df3ec698bc1721"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthenticationModule-4324a033f778707528df3ec698bc1721"' :
                                        'id="xs-injectables-links-module-AuthenticationModule-4324a033f778707528df3ec698bc1721"' }>
                                        <li class="link">
                                            <a href="injectables/AuthenticationService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthenticationService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/TokenStorage.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>TokenStorage</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AuthModule-ae1b595abd2b99f870a8b3e4a2ff53da"' : 'data-target="#xs-components-links-module-AuthModule-ae1b595abd2b99f870a8b3e4a2ff53da"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AuthModule-ae1b595abd2b99f870a8b3e4a2ff53da"' :
                                            'id="xs-components-links-module-AuthModule-ae1b595abd2b99f870a8b3e4a2ff53da"' }>
                                            <li class="link">
                                                <a href="components/AuthComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AuthNoticeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthNoticeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/BuilderModule.html" data-type="entity-link">BuilderModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-BuilderModule-2edd0e193d25f0f4cc610f74b38a2057"' : 'data-target="#xs-components-links-module-BuilderModule-2edd0e193d25f0f4cc610f74b38a2057"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-BuilderModule-2edd0e193d25f0f4cc610f74b38a2057"' :
                                            'id="xs-components-links-module-BuilderModule-2edd0e193d25f0f4cc610f74b38a2057"' }>
                                            <li class="link">
                                                <a href="components/BuilderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BuilderComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CodePreviewModule.html" data-type="entity-link">CodePreviewModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CodePreviewModule-b438d1392b7599ec0526520b4db97723"' : 'data-target="#xs-components-links-module-CodePreviewModule-b438d1392b7599ec0526520b4db97723"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CodePreviewModule-b438d1392b7599ec0526520b4db97723"' :
                                            'id="xs-components-links-module-CodePreviewModule-b438d1392b7599ec0526520b4db97723"' }>
                                            <li class="link">
                                                <a href="components/CodePreviewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CodePreviewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CodePreviewInnerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CodePreviewInnerComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CommunityModule.html" data-type="entity-link">CommunityModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CommunityModule-8155484c4abb3c43d146fe56cca14b47"' : 'data-target="#xs-components-links-module-CommunityModule-8155484c4abb3c43d146fe56cca14b47"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CommunityModule-8155484c4abb3c43d146fe56cca14b47"' :
                                            'id="xs-components-links-module-CommunityModule-8155484c4abb3c43d146fe56cca14b47"' }>
                                            <li class="link">
                                                <a href="components/TimelineComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimelineComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CommunityRoutingModule.html" data-type="entity-link">CommunityRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CompanyModule.html" data-type="entity-link">CompanyModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CompanyModule-705914b49daad8d44ca04c8ced3d13cf"' : 'data-target="#xs-components-links-module-CompanyModule-705914b49daad8d44ca04c8ced3d13cf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CompanyModule-705914b49daad8d44ca04c8ced3d13cf"' :
                                            'id="xs-components-links-module-CompanyModule-705914b49daad8d44ca04c8ced3d13cf"' }>
                                            <li class="link">
                                                <a href="components/BranchesDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BranchesDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CompanyContactComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CompanyContactComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CompanyProfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CompanyProfileComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CompanypositionDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CompanypositionDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ListComponentBranch.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListComponentBranch</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewPositionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewPositionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UpdatePositionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UpdatePositionComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-CoreModule-19ee60a2d72f54d06445158d6935161e"' : 'data-target="#xs-directives-links-module-CoreModule-19ee60a2d72f54d06445158d6935161e"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-CoreModule-19ee60a2d72f54d06445158d6935161e"' :
                                        'id="xs-directives-links-module-CoreModule-19ee60a2d72f54d06445158d6935161e"' }>
                                        <li class="link">
                                            <a href="directives/ClipboardDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">ClipboardDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/HeaderDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">HeaderDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/InputDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">InputDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/MenuAsideDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuAsideDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/MenuAsideOffcanvasDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuAsideOffcanvasDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/MenuAsideToggleDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuAsideToggleDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/MenuHorizontalDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuHorizontalDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/MenuHorizontalOffcanvasDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuHorizontalOffcanvasDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/PortletDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">PortletDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/QuickSearchDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">QuickSearchDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/QuickSidebarOffcanvasDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">QuickSidebarOffcanvasDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/ScrollTopDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScrollTopDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/SelectsplitterDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">SelectsplitterDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-CoreModule-19ee60a2d72f54d06445158d6935161e"' : 'data-target="#xs-pipes-links-module-CoreModule-19ee60a2d72f54d06445158d6935161e"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-CoreModule-19ee60a2d72f54d06445158d6935161e"' :
                                            'id="xs-pipes-links-module-CoreModule-19ee60a2d72f54d06445158d6935161e"' }>
                                            <li class="link">
                                                <a href="pipes/ConsoleLogPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConsoleLogPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/DomainPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DomainPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/FirstLetterPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FirstLetterPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/GetObjectPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GetObjectPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/JoinPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">JoinPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/SafePipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SafePipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/TimeElapsedPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimeElapsedPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CreateModule.html" data-type="entity-link">CreateModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CreateModule-6d995cb5f1a77a625b1bb03aa19a43af"' : 'data-target="#xs-components-links-module-CreateModule-6d995cb5f1a77a625b1bb03aa19a43af"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CreateModule-6d995cb5f1a77a625b1bb03aa19a43af"' :
                                            'id="xs-components-links-module-CreateModule-6d995cb5f1a77a625b1bb03aa19a43af"' }>
                                            <li class="link">
                                                <a href="components/CreateComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CreateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CreateRoutingModule.html" data-type="entity-link">CreateRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardModule.html" data-type="entity-link">DashboardModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DashboardModule-81cc34705c005519683754b8f306f52a"' : 'data-target="#xs-components-links-module-DashboardModule-81cc34705c005519683754b8f306f52a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DashboardModule-81cc34705c005519683754b8f306f52a"' :
                                            'id="xs-components-links-module-DashboardModule-81cc34705c005519683754b8f306f52a"' }>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DashboardComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardModule.html" data-type="entity-link">DashboardModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardRoutingModule.html" data-type="entity-link">DashboardRoutingModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DashboardRoutingModule-0399ba2eaaca14a39cedf0e72e17a73c"' : 'data-target="#xs-components-links-module-DashboardRoutingModule-0399ba2eaaca14a39cedf0e72e17a73c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DashboardRoutingModule-0399ba2eaaca14a39cedf0e72e17a73c"' :
                                            'id="xs-components-links-module-DashboardRoutingModule-0399ba2eaaca14a39cedf0e72e17a73c"' }>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DashboardComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeModule.html" data-type="entity-link">HomeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeModule-76811d275c821a388759214e88e0c90e"' : 'data-target="#xs-components-links-module-HomeModule-76811d275c821a388759214e88e0c90e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeModule-76811d275c821a388759214e88e0c90e"' :
                                            'id="xs-components-links-module-HomeModule-76811d275c821a388759214e88e0c90e"' }>
                                            <li class="link">
                                                <a href="components/CompaniesDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CompaniesDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ContactsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ContactsDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactsDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ContactsDetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactsDetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ContactsdetailsDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactsdetailsDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MapComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MapComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PositionsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PositionsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PositionsDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PositionsDatatableComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LayoutModule.html" data-type="entity-link">LayoutModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LayoutModule-48164d9373861a630e0660a5b81e9e0e"' : 'data-target="#xs-components-links-module-LayoutModule-48164d9373861a630e0660a5b81e9e0e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LayoutModule-48164d9373861a630e0660a5b81e9e0e"' :
                                            'id="xs-components-links-module-LayoutModule-48164d9373861a630e0660a5b81e9e0e"' }>
                                            <li class="link">
                                                <a href="components/AsideLeftComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AsideLeftComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AsideRightComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AsideRightComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BrandComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BrandComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FooterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderSearchComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HeaderSearchComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LanguageSelectorComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LanguageSelectorComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MenuHorizontalComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuHorizontalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MenuSectionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuSectionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotificationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/QuickActionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">QuickActionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchDefaultComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchDefaultComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchDropdownComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchDropdownComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SubheaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SubheaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TopbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TopbarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserProfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserProfileComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ListTimelineModule.html" data-type="entity-link">ListTimelineModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ListTimelineModule-2b2ddcde16f0d840647d4797cc8421f1"' : 'data-target="#xs-components-links-module-ListTimelineModule-2b2ddcde16f0d840647d4797cc8421f1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ListTimelineModule-2b2ddcde16f0d840647d4797cc8421f1"' :
                                            'id="xs-components-links-module-ListTimelineModule-2b2ddcde16f0d840647d4797cc8421f1"' }>
                                            <li class="link">
                                                <a href="components/ListTimelineComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListTimelineComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TimelineItemComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimelineItemComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialPreviewModule.html" data-type="entity-link">MaterialPreviewModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MaterialPreviewModule-de15047fa2d2365b2b807d4d92281d45"' : 'data-target="#xs-components-links-module-MaterialPreviewModule-de15047fa2d2365b2b807d4d92281d45"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MaterialPreviewModule-de15047fa2d2365b2b807d4d92281d45"' :
                                            'id="xs-components-links-module-MaterialPreviewModule-de15047fa2d2365b2b807d4d92281d45"' }>
                                            <li class="link">
                                                <a href="components/MaterialPreviewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MaterialPreviewComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MessengerModule.html" data-type="entity-link">MessengerModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MessengerModule-4b1e1b1cfc2b4c565e5ec66975eae646"' : 'data-target="#xs-components-links-module-MessengerModule-4b1e1b1cfc2b4c565e5ec66975eae646"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MessengerModule-4b1e1b1cfc2b4c565e5ec66975eae646"' :
                                            'id="xs-components-links-module-MessengerModule-4b1e1b1cfc2b4c565e5ec66975eae646"' }>
                                            <li class="link">
                                                <a href="components/MessengerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MessengerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MessengerInComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MessengerInComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MessengerOutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MessengerOutComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NewsModule.html" data-type="entity-link">NewsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NewsModule-072c327cb959300b5b9af9b551faf4db"' : 'data-target="#xs-components-links-module-NewsModule-072c327cb959300b5b9af9b551faf4db"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NewsModule-072c327cb959300b5b9af9b551faf4db"' :
                                            'id="xs-components-links-module-NewsModule-072c327cb959300b5b9af9b551faf4db"' }>
                                            <li class="link">
                                                <a href="components/CategoryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CategoryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CategoryDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CategoryDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CategoryEditComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CategoryEditComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CategoryFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CategoryFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CreateGalleryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CreateGalleryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CreateNewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CreateNewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GalleryDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GalleryDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GalleryEditComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GalleryEditComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GalleryFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GalleryFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewsFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewsFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewsListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewsListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewsListDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewsListDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UpdateNewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UpdateNewComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationModule.html" data-type="entity-link">NotificationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotificationModule-58f6cda059fc880e5157ebcdb6adfb24"' : 'data-target="#xs-components-links-module-NotificationModule-58f6cda059fc880e5157ebcdb6adfb24"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotificationModule-58f6cda059fc880e5157ebcdb6adfb24"' :
                                            'id="xs-components-links-module-NotificationModule-58f6cda059fc880e5157ebcdb6adfb24"' }>
                                            <li class="link">
                                                <a href="components/NewNotificationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewNotificationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotificationDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotificationListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PagesModule.html" data-type="entity-link">PagesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PagesModule-a6bc37365e0ba253cb1b38476368705e"' : 'data-target="#xs-components-links-module-PagesModule-a6bc37365e0ba253cb1b38476368705e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PagesModule-a6bc37365e0ba253cb1b38476368705e"' :
                                            'id="xs-components-links-module-PagesModule-a6bc37365e0ba253cb1b38476368705e"' }>
                                            <li class="link">
                                                <a href="components/CodeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CodeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ErrorPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ErrorPageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ForgotPasswordComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ForgotPasswordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PagesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PagesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RegisterComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PagesRoutingModule.html" data-type="entity-link">PagesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PartialsModule.html" data-type="entity-link">PartialsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PartialsModule-4c38cb56345d461fade4a3d57d2cee87"' : 'data-target="#xs-components-links-module-PartialsModule-4c38cb56345d461fade4a3d57d2cee87"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PartialsModule-4c38cb56345d461fade4a3d57d2cee87"' :
                                            'id="xs-components-links-module-PartialsModule-4c38cb56345d461fade4a3d57d2cee87"' }>
                                            <li class="link">
                                                <a href="components/AuditLogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuditLogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AuthorProfitComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthorProfitComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BestSellerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BestSellerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BlogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BlogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BranchSelectComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BranchSelectComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardHeaderWithButtonComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CardHeaderWithButtonComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CommonValidationsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CommonValidationsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ContactCompanyComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactCompanyComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ContactListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ContactPositionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactPositionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CounterBannerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CounterBannerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DataTableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DataTableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DatatableFiltersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DatatableFiltersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DatatableToggleComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DatatableToggleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EmptyViewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EmptyViewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FileWithPreviewComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FileWithPreviewComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FileuploadComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FileuploadComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FinanceStatsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FinanceStatsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GoogleAuthComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GoogleAuthComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GoogleRegisterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GoogleRegisterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GroupSelectComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GroupSelectComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InputWithIconComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InputWithIconComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ListSettingsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListSettingsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NoticeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NoticeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PackagesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PackagesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PositionSelectComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PositionSelectComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/QuickSidebarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">QuickSidebarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RecentActivitiesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RecentActivitiesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RecentNotificationsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RecentNotificationsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ScrollTopComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScrollTopComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SpinnerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SpinnerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StatComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StatComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SupportTicketsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SupportTicketsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TasksComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TasksComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TooltipsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TooltipsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserDataTableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserDataTableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserGroupDatatableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserGroupDatatableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserSelectComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserSelectComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PortletModule.html" data-type="entity-link">PortletModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PortletModule-18526e95470ed44185228b59b84ecbad"' : 'data-target="#xs-components-links-module-PortletModule-18526e95470ed44185228b59b84ecbad"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PortletModule-18526e95470ed44185228b59b84ecbad"' :
                                            'id="xs-components-links-module-PortletModule-18526e95470ed44185228b59b84ecbad"' }>
                                            <li class="link">
                                                <a href="components/PortletComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PortletComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProfileModule.html" data-type="entity-link">ProfileModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProfileModule-2ada734b815b3305e548859386b84ef9"' : 'data-target="#xs-components-links-module-ProfileModule-2ada734b815b3305e548859386b84ef9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProfileModule-2ada734b815b3305e548859386b84ef9"' :
                                            'id="xs-components-links-module-ProfileModule-2ada734b815b3305e548859386b84ef9"' }>
                                            <li class="link">
                                                <a href="components/ProfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProfileComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProfileRoutingModule.html" data-type="entity-link">ProfileRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/QuotesModule.html" data-type="entity-link">QuotesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-QuotesModule-511a277ced310e5463d0054403fcb3f4"' : 'data-target="#xs-components-links-module-QuotesModule-511a277ced310e5463d0054403fcb3f4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-QuotesModule-511a277ced310e5463d0054403fcb3f4"' :
                                            'id="xs-components-links-module-QuotesModule-511a277ced310e5463d0054403fcb3f4"' }>
                                            <li class="link">
                                                <a href="components/QuotesFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">QuotesFormComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/QuotesRoutingModule.html" data-type="entity-link">QuotesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SpinnerButtonModule.html" data-type="entity-link">SpinnerButtonModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SpinnerButtonModule-d76e171bde4565bcca9aaf40072577db"' : 'data-target="#xs-components-links-module-SpinnerButtonModule-d76e171bde4565bcca9aaf40072577db"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SpinnerButtonModule-d76e171bde4565bcca9aaf40072577db"' :
                                            'id="xs-components-links-module-SpinnerButtonModule-d76e171bde4565bcca9aaf40072577db"' }>
                                            <li class="link">
                                                <a href="components/SpinnerButtonComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SpinnerButtonComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link">UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UsersModule-dc80ca4a559337702b8e734a7b4fb945"' : 'data-target="#xs-components-links-module-UsersModule-dc80ca4a559337702b8e734a7b4fb945"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UsersModule-dc80ca4a559337702b8e734a7b4fb945"' :
                                            'id="xs-components-links-module-UsersModule-dc80ca4a559337702b8e734a7b4fb945"' }>
                                            <li class="link">
                                                <a href="components/EditGroupComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EditGroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GroupFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GroupFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewUserComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewUserComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UpdateUserComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UpdateUserComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UploadUsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UploadUsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserGroupMembersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserGroupMembersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserGroupsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserGroupsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/WidgetChartsModule.html" data-type="entity-link">WidgetChartsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-WidgetChartsModule-d0fb96f72b0e4fd15e08aeba195f93f6"' : 'data-target="#xs-components-links-module-WidgetChartsModule-d0fb96f72b0e4fd15e08aeba195f93f6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-WidgetChartsModule-d0fb96f72b0e4fd15e08aeba195f93f6"' :
                                            'id="xs-components-links-module-WidgetChartsModule-d0fb96f72b0e4fd15e08aeba195f93f6"' }>
                                            <li class="link">
                                                <a href="components/BarChartComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BarChartComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DoughnutChartComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DoughnutChartComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MapChartComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MapChartComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/DashboardComponent-1.html" data-type="entity-link">DashboardComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AclModel.html" data-type="entity-link">AclModel</a>
                            </li>
                            <li class="link">
                                <a href="classes/DataTableDataSource.html" data-type="entity-link">DataTableDataSource</a>
                            </li>
                            <li class="link">
                                <a href="classes/DataTableItemModel.html" data-type="entity-link">DataTableItemModel</a>
                            </li>
                            <li class="link">
                                <a href="classes/ExternalCodeExample.html" data-type="entity-link">ExternalCodeExample</a>
                            </li>
                            <li class="link">
                                <a href="classes/ImDataTable.html" data-type="entity-link">ImDataTable</a>
                            </li>
                            <li class="link">
                                <a href="classes/ImDatatableWrapper.html" data-type="entity-link">ImDatatableWrapper</a>
                            </li>
                            <li class="link">
                                <a href="classes/LayoutConfig.html" data-type="entity-link">LayoutConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/MenuConfig.html" data-type="entity-link">MenuConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/PagesConfig.html" data-type="entity-link">PagesConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/QueryParamsModel.html" data-type="entity-link">QueryParamsModel</a>
                            </li>
                            <li class="link">
                                <a href="classes/QueryResultsModel.html" data-type="entity-link">QueryResultsModel</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AccordionControlConfig.html" data-type="entity-link">AccordionControlConfig</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AclService.html" data-type="entity-link">AclService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthenticationService.html" data-type="entity-link">AuthenticationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthNoticeService.html" data-type="entity-link">AuthNoticeService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BranchesService.html" data-type="entity-link">BranchesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ClassInitService.html" data-type="entity-link">ClassInitService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ClipboardService.html" data-type="entity-link">ClipboardService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CommunityService.html" data-type="entity-link">CommunityService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CompanyService.html" data-type="entity-link">CompanyService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DashboardService.html" data-type="entity-link">DashboardService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DataTableService.html" data-type="entity-link">DataTableService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FilesService.html" data-type="entity-link">FilesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ForgotPasswordService.html" data-type="entity-link">ForgotPasswordService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HeaderService.html" data-type="entity-link">HeaderService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/HttpUtilsService.html" data-type="entity-link">HttpUtilsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LaravelValidationsService.html" data-type="entity-link">LaravelValidationsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayoutConfigService.html" data-type="entity-link">LayoutConfigService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayoutConfigStorageService.html" data-type="entity-link">LayoutConfigStorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayoutRefService.html" data-type="entity-link">LayoutRefService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoadingService.html" data-type="entity-link">LoadingService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LogsService.html" data-type="entity-link">LogsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MenuAsideService.html" data-type="entity-link">MenuAsideService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MenuConfigService.html" data-type="entity-link">MenuConfigService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MenuHorizontalService.html" data-type="entity-link">MenuHorizontalService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MessengerService.html" data-type="entity-link">MessengerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NewsService.html" data-type="entity-link">NewsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationsService.html" data-type="entity-link">NotificationsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PageConfigService.html" data-type="entity-link">PageConfigService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PositionsService.html" data-type="entity-link">PositionsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/QuickSearchService.html" data-type="entity-link">QuickSearchService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SplashScreenService.html" data-type="entity-link">SplashScreenService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SubheaderService.html" data-type="entity-link">SubheaderService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TokenStorage.html" data-type="entity-link">TokenStorage</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TranslationService.html" data-type="entity-link">TranslationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link">UserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UtilsService.html" data-type="entity-link">UtilsService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/AuthInterceptor.html" data-type="entity-link">AuthInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/AccessData.html" data-type="entity-link">AccessData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AccordionControlPanelChangeEvent.html" data-type="entity-link">AccordionControlPanelChangeEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AclInterface.html" data-type="entity-link">AclInterface</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AuthNotice.html" data-type="entity-link">AuthNotice</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Breadcrumb.html" data-type="entity-link">Breadcrumb</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ConfigData.html" data-type="entity-link">ConfigData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ConfigModel.html" data-type="entity-link">ConfigModel</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Credential.html" data-type="entity-link">Credential</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LanguageFlag.html" data-type="entity-link">LanguageFlag</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Locale.html" data-type="entity-link">Locale</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LogData.html" data-type="entity-link">LogData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/marker.html" data-type="entity-link">marker</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/marker-1.html" data-type="entity-link">marker</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MessageData.html" data-type="entity-link">MessageData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PortletOptions.html" data-type="entity-link">PortletOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SpinnerButtonOptions.html" data-type="entity-link">SpinnerButtonOptions</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});