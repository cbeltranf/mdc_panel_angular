export const environment = {
  production: true,
  apiUrl: 'https://inmotechapi.mydigitalcard.us/api/v1',
  assetsDomain: 'https://inmotechapi.mydigitalcard.us',
  isMockEnabled: true, // You have to switch this, when your real back-end is done
  googleClientId: '181839308556-05h61qir5vgd14okpf136o41uhq94l3e.apps.googleusercontent.com',
};
